(function () {
    'use strict';

    var serviceApp = angular.module('myapp', ['ngRoute']);

    var url = 'http://192.168.0.101/CalculatorMonopolyGame/Site/';

    serviceApp.config(['$routeProvider',
        function ($routeProvider, $locationProvider) {
            $routeProvider.
                when('/game', { templateUrl: url + 'game.html', controller: 'GameController' }).
                when('/balance', { templateUrl: url + 'balance.html', controller: 'BalanceController' }).
                when('/historyMove', { templateUrl: url + 'historyMove.html', controller: 'HistoryMoveController' }).
                otherwise({
                    redirectTo: '/'
                });
            //$locationProvider.html5Mode(true);
        }
    ]);

    // habilita CORS 
    serviceApp.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }]);
})();