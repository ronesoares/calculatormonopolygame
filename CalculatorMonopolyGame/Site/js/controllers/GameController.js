(function(){
    'use strict';

    angular
    .module('myapp')
    .controller('GameController', function($scope, ServiceApi, $location, $anchorScroll) {
        var vm = this;

        $scope.GameLists = [];

        $scope.GameSelected = function(id, description) {
            ServiceApi.GameSelected = {id: id, description: description};
        }
                
        var loadGameList = function() {             
            ServiceApi.GameList().then(function(GameLists) {
                $scope.GameLists = GameLists;
                
                setTimeout(function() {
                    $location.hash('goView');
                    $anchorScroll();
                }, 1);
            });
        }

        $scope.GameList = function(){
            loadGameList();
        }

        $scope.GameInsert = function() {
            ServiceApi.GameInsert($scope.GameInserting).then(loadGameList);
        }

        $scope.GameUpdate = function(gamelist) {
            ServiceApi.GameUpdate(gamelist).then(loadGameList);
        }

        $scope.GameDelete = function(id) {
            ServiceApi.GameDelete(id).then(loadGameList);
        }

        loadGameList();
    });

})();