(function(){
    'use strict';

    angular
    .module('myapp')
    .controller('HistoryMoveController', function($scope, ServiceApi, $location, $anchorScroll) {
                
        $scope.BalanceLists = [];
        $scope.GameSelected = [];
        $scope.CardLists = [];
        $scope.HistoryMoveLists = [];
                
        var loadHistoryMoveList = function() {             
            ServiceApi.HistoryMoveList(ServiceApi.GameSelected.id).then(function(HistoryMoveLists) {
                $scope.HistoryMoveLists = HistoryMoveLists;
                $scope.GameSelected = ServiceApi.GameSelected;
                loadCardList();

                ServiceApi.BalanceList(ServiceApi.GameSelected.id).then(function(BalanceLists) {
                    $scope.BalanceLists = BalanceLists;                
                });
                
                setTimeout(function() {
                    $location.hash('goView');
                    $anchorScroll();
                }, 1);
            });
        }

        $scope.HistoryMoveList = function(){
            loadHistoryMoveList();
        }

        $scope.HistoryMoveInsert = function() {
            $scope.HistoryMoveInserting.CodGame = ServiceApi.GameSelected.id;            

            ServiceApi.HistoryMoveInsert($scope.HistoryMoveInserting).then(loadHistoryMoveList); 

            $scope.HistoryMoveInserting = [];
        }

        $scope.HistoryMoveDelete = function(codHistoryMove) {
            ServiceApi.HistoryMoveDelete(codHistoryMove).then(loadHistoryMoveList);
        }

        var loadCardList = function() {             
            ServiceApi.CardList().then(function(CardLists) {
                $scope.CardLists = CardLists;
            });
        }

        loadHistoryMoveList();
    });

})();