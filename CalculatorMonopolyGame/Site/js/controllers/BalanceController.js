(function(){
    'use strict';

    angular
    .module('myapp')
    .controller('BalanceController', function($scope, ServiceApi, $location, $anchorScroll) {
        
        $scope.BalanceLists = [];
        $scope.GameSelected = [];
        $scope.CardLists = [];
                
        var loadBalanceList = function() {             
            ServiceApi.BalanceList(ServiceApi.GameSelected.id).then(function(BalanceLists) {
                $scope.BalanceLists = BalanceLists;
                $scope.GameSelected = ServiceApi.GameSelected;
                loadCardList();
                
                setTimeout(function() {
                    $location.hash('goView');
                    $anchorScroll();
                }, 1);
            });
        }

        $scope.BalanceList = function(){
            loadBalanceList();
        }

        $scope.BalanceInsert = function() {
            $scope.BalanceInserting.CodGame = ServiceApi.GameSelected.id;
            $scope.BalanceInserting.ValTotal = $scope.BalanceInserting.ValStarted;

            ServiceApi.BalanceInsert($scope.BalanceInserting).then(loadBalanceList); 
        }

        $scope.BalanceDelete = function(codBalance) {
            ServiceApi.BalanceDelete(codBalance).then(loadBalanceList);
        }

        var loadCardList = function() {             
            ServiceApi.CardList().then(function(CardLists) {
                $scope.CardLists = CardLists;
            });
        }

        loadBalanceList();
    });

})();