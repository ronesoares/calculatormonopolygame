angular
    .module('myapp')
    .factory('ServiceApi', function($q, $http){
        var GameSelected;
        var url = 'http://192.168.0.101/CalculatorMonopolyGame';

        return {
            /*Begin Game*/
            GameList : function() {
                var list = $q.defer();

                $http.get(url + '/Game/List').then(
                    function(result){
                        var gamelist = [];

                        gamelist = result.data;

                        list.resolve(gamelist);
                    }
                );
                
                return list.promise;
            },

            GameInsert : function(GameList) {
                return $http.post(url + '/Game/Insert', GameList);
            },

            GameUpdate : function(GameList) {
                return $http.put(url + '/Game/Update', GameList);
            },

            GameDelete : function(id) {
                return $http.delete(url + '/Game/Delete/' + id);
            },
            /*End Game*/

            /*Begin Balance*/
            BalanceList : function(codGame) {
                var list = $q.defer();

                $http.get(url + '/Balance/List/' + codGame).then(
                    function(result){
                        var balancelist = [];

                        balancelist = result.data;

                        list.resolve(balancelist);
                    }
                );
                
                return list.promise;
            },

            BalanceInsert : function(BalanceList) {
                return $http.post(url + '/Balance/Insert', BalanceList);
            },

            BalanceDelete : function(codBalance) {
                return $http.delete(url + '/Balance/Delete/' + codBalance);
            },
            /*End Balance*/ 

            /*Begin Card*/
            CardList : function() {
                var list = $q.defer();

                $http.get(url + '/Card/List').then(
                    function(result){
                        var cardlist = [];

                        cardlist = result.data;

                        list.resolve(cardlist);
                    }
                );
                
                return list.promise;
            },
            /*End Card*/

            /*Begin HistoryMove*/
            HistoryMoveList : function(codGame) {
                var list = $q.defer();

                $http.get(url + '/HistoryMove/List/' + codGame).then(
                    function(result){
                        var historyMovelist = [];

                        historyMovelist = result.data;

                        list.resolve(historyMovelist);
                    }
                );
                
                return list.promise;
            },

            HistoryMoveInsert : function(HistoryMoveList) {
                return $http.post(url + '/HistoryMove/Insert', HistoryMoveList);
            },

            HistoryMoveDelete : function(codHistoryMove) {
                return $http.delete(url + '/HistoryMove/Delete/' + codHistoryMove);
            },
            /*End HistoryMove*/
        };
    });