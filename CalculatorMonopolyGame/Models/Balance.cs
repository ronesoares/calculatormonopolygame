﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalculatorMonopolyGame.Models
{
    public class Balance
    {
        public int CodBalance { get; set; }
        public int CodGame { get; set; }
        public int CodCard { get; set; }
        public float ValTotal { get; set; }
        public float ValStarted { get; set; }

        public Balance() { }

        public Balance(int codGame, int codCard, float valTotal, float valStarted)
        {
            if (codCard > 0)
            {
                CodCard = codCard;
            }
            else
            {
                throw new Exception("You must declare a Card.");
            }

            if (codGame > 0)
            {
                CodGame = codGame;
            }
            else
            {
                throw new Exception("You must declare a Game.");
            }

            if (valTotal > 0)
            {
                ValTotal = valTotal;
            }
            else
            {
                throw new Exception("You must declare a Total Value larger that zero.");
            }

            if (valStarted > 0)
            {
                ValStarted = valStarted;
            }
            else
            {
                throw new Exception("You must declare a Started Value larger that zero.");
            }
        }
    }
}