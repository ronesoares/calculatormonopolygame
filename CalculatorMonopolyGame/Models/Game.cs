﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalculatorMonopolyGame.Models
{
    public class Game
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }

        public Game() {}

        public Game(string description)
        {
            SetDescription(description);
            Date = DateTime.Now;
        }

        private void SetDescription(string description)
        {
            if (description == null)
            {
                throw new System.NullReferenceException("The description Game can't to be null.");
            }

            if (description.Length > 20)
            {
                throw new System.OverflowException("The description Game exceeded the 20 character limit.");
            }

            Description = description;
        }
    }
}