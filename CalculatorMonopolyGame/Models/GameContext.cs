﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirebirdSql.Data.FirebirdClient;
using System.Data;

namespace CalculatorMonopolyGame.Models
{
    public class GameContext
    {
        ConnectionDB connectionDB = new ConnectionDB();

        public IEnumerable<Game> Games
        {
            get
            {
                List<Game> games = new List<Game>();
                                
                string commandText = @"SELECT * 
                                         FROM MON_GAME
                                        ORDER BY DSCGAME ";

                FbCommand fbCommand = connectionDB.prepareconnection(commandText);

                FbDataReader rdr = fbCommand.ExecuteReader();
                while (rdr.Read())
                {
                    Game game = new Game();
                    game.Id = Convert.ToInt32(rdr["CodGame"]);
                    game.Description = rdr["DscGame"].ToString();

                    if (!(rdr["DatGame"] is DBNull))
                    {
                        game.Date = Convert.ToDateTime(rdr["DatGame"]);
                    }

                    games.Add(game);
                }
                                
                fbCommand.Dispose();

                return games;
            }
        }

        public int postGames(string description)
        {
            Game games = new Game(description);

            string commandText = @"SELECT GEN_ID(GEN_MON_GAME_ID,1) AS CODGAME
                                     FROM DUAL";

            FbCommand fbSequence = connectionDB.prepareconnection(commandText);
            FbDataReader rdr = fbSequence.ExecuteReader();

            if (rdr.Read())
            {
                games.Id = Convert.ToInt32(rdr["CodGame"]);
            }
                        
            fbSequence.Dispose();
            

            commandText = @"INSERT INTO MON_GAME (CODGAME, DSCGAME, DATGAME) 
                                          VALUES (@CODGAME, @DSCGAME, @DATGAME) ";

            FbCommand fbCommand = connectionDB.prepareconnection(commandText);
            fbCommand.Parameters.AddWithValue("CODGAME", games.Id);
            fbCommand.Parameters.AddWithValue("DSCGAME", games.Description);
            fbCommand.Parameters.AddWithValue("DATGAME", games.Date);
                        
            fbCommand.ExecuteNonQuery();
            fbCommand.Transaction.Commit();            
            fbCommand.Dispose();
            
            return games.Id;
        }

        public void deleteGames(int id)
        {
            string commandText = @"DELETE FROM MON_GAME
                                    WHERE CODGAME = @CODGAME ";

            FbCommand fbCommand = connectionDB.prepareconnection(commandText);
            fbCommand.Parameters.AddWithValue("CODGAME", id);
                        
            fbCommand.ExecuteNonQuery();
            fbCommand.Transaction.Commit();            
            fbCommand.Dispose();
        }

        public void putGames(Game games)
        {
            string commandText = @"UPDATE MON_GAME 
                                      SET DSCGAME = @DSCGAME,
                                          DATGAME = @DATGAME
                                    WHERE CODGAME = @CODGAME ";

            FbCommand fbCommand = connectionDB.prepareconnection(commandText);
            fbCommand.Parameters.AddWithValue("CODGAME", games.Id);
            fbCommand.Parameters.AddWithValue("DSCGAME", games.Description);
            fbCommand.Parameters.AddWithValue("DATGAME", games.Date);
                        
            fbCommand.ExecuteNonQuery();
            fbCommand.Transaction.Commit();            
            fbCommand.Dispose();
        }
    }
}