﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalculatorMonopolyGame.Models
{
    public class HistoryMove
    {
        public int CodHistoryMove { get; set; }
        public int CodCardDebit { get; set; }
        public int CodCardCredit { get; set; }
        public int CodGame { get; set; }
        public float ValTotal { get; set; }
        public DateTime DatMove { get; set; }

        public HistoryMove() { }

        public HistoryMove(int codCardDebit, int codCardCredit, int codGame, float valTotal)
        {
            DatMove = DateTime.Now;

            if (codCardDebit > 0 || codCardCredit > 0)
            {
                CodCardDebit = codCardDebit;
                CodCardCredit = codCardCredit;
            }
            else
            {
                throw new Exception("You must declare Card Debit or Card Credit.");
            }
            
            if (codGame > 0)
            {
                CodGame = codGame;
            }
            else
            {
                throw new Exception("You must declare a Game."); 
            }

            if (valTotal > 0)
            {
                ValTotal = valTotal;
            }
            else
            {
                throw new Exception("You must declare a Total Value larger that zero.");
            }
        }
    }
}