﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalculatorMonopolyGame.Models
{
    public class Card
    {
        public int CodCard { get; set; }
        public string DscCard { get; set; }

        public Card() { }

        public Card(string dscCard)
        {
            SetDscCard(dscCard);
        }

        private void SetDscCard(string dscCard)
        {
            if (dscCard == null)
            {
                throw new System.NullReferenceException("The description Card can't to be null.");
            }

            if (dscCard.Length > 20)
            {
                throw new System.OverflowException("The description Card exceeded the 20 character limit.");
            }

            DscCard = dscCard;
        }
    }
}