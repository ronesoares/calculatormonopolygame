﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirebirdSql.Data.FirebirdClient;
using System.Data;

namespace CalculatorMonopolyGame.Models
{
    public class CardContext
    {
        ConnectionDB connectionDB = new ConnectionDB();

        public IEnumerable<Card> Cards
        {
            get
            {
                List<Card> cards = new List<Card>();

                string commandText = @"SELECT * 
                                         FROM MON_CARD
                                        WHERE CODCARD <> 0
                                        ORDER BY DSCCARD ";
                FbCommand fbCommand = connectionDB.prepareconnection(commandText);
                
                FbDataReader rdr = fbCommand.ExecuteReader();
                while (rdr.Read())
                {
                    Card card = new Card();
                    card.CodCard = Convert.ToInt32(rdr["CodCard"]);
                    card.DscCard = rdr["DscCard"].ToString();

                    cards.Add(card);
                }

                fbCommand.Dispose();

                return cards;
            }
        }

        public int postCards(string dscCard)
        {
            Card cards = new Card(dscCard);

            string commandText = @"SELECT GEN_ID(GEN_MON_CARD_ID,1) AS CODCARD
                                     FROM DUAL";

            FbCommand fbSequence = connectionDB.prepareconnection(commandText);
            FbDataReader rdr = fbSequence.ExecuteReader();

            if (rdr.Read())
            {
                cards.CodCard = Convert.ToInt32(rdr["CodCard"]);
            }

            fbSequence.Dispose();


            commandText = @"INSERT INTO MON_CARD (CODCARD, DSCCARD) 
                                          VALUES (@CODCARD, @DSCCARD) ";

            FbCommand fbCommand = connectionDB.prepareconnection(commandText);
            fbCommand.Parameters.AddWithValue("CODCARD", cards.CodCard);
            fbCommand.Parameters.AddWithValue("DSCCARD", cards.DscCard);
                        
            fbCommand.ExecuteNonQuery();
            fbCommand.Transaction.Commit();
            fbCommand.Dispose();

            return cards.CodCard;
        }

        public void deleteCards(int codCards)
        {
            string commandText = @"DELETE FROM MON_CARD 
                                    WHERE CODCARD = @CODCARD ";

            FbCommand fbCommand = connectionDB.prepareconnection(commandText);
            fbCommand.Parameters.AddWithValue("CODCARD", codCards);
                        
            fbCommand.ExecuteNonQuery();
            fbCommand.Transaction.Commit();
            fbCommand.Dispose();
        }

        public void putCards(Card cards)
        {
            string commandText = @"UPDATE MON_CARD 
                                      SET DSCCARD = @DSCCARD 
                                    WHERE CODCARD = @CODCARD ";

            FbCommand fbCommand = connectionDB.prepareconnection(commandText);
            fbCommand.Parameters.AddWithValue("CODCARD", cards.CodCard);
            fbCommand.Parameters.AddWithValue("DSCCARD", cards.DscCard);
                        
            fbCommand.ExecuteNonQuery();
            fbCommand.Transaction.Commit();
            fbCommand.Dispose();
        }
    }
}