﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirebirdSql.Data.FirebirdClient;
using System.Data;

namespace CalculatorMonopolyGame.Models
{
    public class HistoryMoveContext
    {
        ConnectionDB connectionDB = new ConnectionDB();

        public IEnumerable<HistoryMove> HistoryMoves (int codGame, int codHistoryMove)
        {
            List<HistoryMove> historyMoves = new List<HistoryMove>();
                                
            string commandText = @"SELECT * 
                                     FROM MON_HISTORYMOVE 
                                    WHERE CODGAME = @CODGAME
                                       OR CODHISTORYMOVE = @CODHISTORYMOVE
                                    ORDER BY CODHISTORYMOVE DESC ";

            FbCommand fbCommand = connectionDB.prepareconnection(commandText);
            fbCommand.Parameters.AddWithValue("CODGAME", codGame);
            fbCommand.Parameters.AddWithValue("CODHISTORYMOVE", codHistoryMove);

            FbDataReader rdr = fbCommand.ExecuteReader();
            while (rdr.Read())
            {
                HistoryMove historyMove = new HistoryMove();
                historyMove.CodHistoryMove = Convert.ToInt32(rdr["CodHistoryMove"]);
                historyMove.CodCardDebit = Convert.ToInt32(rdr["CodCardDebit"]);
                historyMove.CodCardCredit = Convert.ToInt32(rdr["CodCardCredit"]);
                historyMove.CodGame = Convert.ToInt32(rdr["CodGame"]);
                historyMove.ValTotal = float.Parse(rdr["ValTotal"].ToString());

                if (!(rdr["DatMove"] is DBNull))
                {
                    historyMove.DatMove = Convert.ToDateTime(rdr["DatMove"]);
                }

                historyMoves.Add(historyMove);                    
            }

            fbCommand.Dispose();

            return historyMoves;
            
        }

        public int postHistoryMoves(HistoryMove historyMoves)
        {
            HistoryMove historyMove = new HistoryMove(historyMoves.CodCardDebit,
                                                      historyMoves.CodCardCredit,
                                                      historyMoves.CodGame,
                                                      historyMoves.ValTotal);

            string commandText = @"SELECT GEN_ID(GEN_MON_HISTORYMOVE_ID,1) AS CODHISTORYMOVE
                                     FROM DUAL";

            FbCommand fbSequence = connectionDB.prepareconnection(commandText);
            FbDataReader rdr = fbSequence.ExecuteReader();

            if (rdr.Read())
            {
                historyMove.CodHistoryMove = Convert.ToInt32(rdr["CODHISTORYMOVE"]);
            }

            fbSequence.Dispose();


            commandText = @"INSERT INTO MON_HISTORYMOVE (CODHISTORYMOVE, CODCARDDEBIT, CODCARDCREDIT, 
                                                         VALTOTAL, DATMOVE, CODGAME) 
                                                 VALUES (@CODHISTORYMOVE, @CODCARDDEBIT, @CODCARDCREDIT, 
                                                         @VALTOTAL, @DATMOVE, @CODGAME) ";

            FbCommand fbCommand = connectionDB.prepareconnection(commandText);
            fbCommand.Parameters.AddWithValue("CODHISTORYMOVE", historyMove.CodHistoryMove);
            fbCommand.Parameters.AddWithValue("CODCARDDEBIT", historyMove.CodCardDebit);
            fbCommand.Parameters.AddWithValue("CODCARDCREDIT", historyMove.CodCardCredit);
            fbCommand.Parameters.AddWithValue("VALTOTAL", historyMove.ValTotal);
            fbCommand.Parameters.AddWithValue("DATMOVE", historyMove.DatMove);
            fbCommand.Parameters.AddWithValue("CODGAME", historyMove.CodGame);
                                    
            fbCommand.ExecuteNonQuery();
            fbCommand.Transaction.Commit();
            fbCommand.Dispose();
            
            return historyMove.CodHistoryMove;
        }

        public void deleteHistoryMoves(int codHistoryMoves)
        {
            string commandText = @"DELETE FROM MON_HISTORYMOVE 
                                    WHERE CODHISTORYMOVE = @CODHISTORYMOVE ";

            FbCommand fbCommand = connectionDB.prepareconnection(commandText);
            fbCommand.Parameters.AddWithValue("CODHISTORYMOVE", codHistoryMoves);
                        
            fbCommand.ExecuteNonQuery();
            fbCommand.Transaction.Commit();
            fbCommand.Dispose();
        }

        public void putHistoryMoves(HistoryMove historyMoves)
        {
            string commandText = @"UPDATE MON_HISTORYMOVE 
                                      SET CODCARDDEBIT = @CODCARDDEBIT, 
                                          CODCARDCREDIT = @CODCARDCREDIT, 
                                          VALTOTAL = @VALTOTAL, 
                                          DATMOVE = @DATMOVE, 
                                          CODGAME = @CODGAME 
                                    WHERE CODHISTORYMOVE = @CODHISTORYMOVE ";

            FbCommand fbCommand = connectionDB.prepareconnection(commandText);
            fbCommand.Parameters.AddWithValue("CODHISTORYMOVE", historyMoves.CodHistoryMove);
            fbCommand.Parameters.AddWithValue("CODCARDDEBIT", historyMoves.CodCardDebit);
            fbCommand.Parameters.AddWithValue("CODCARDCREDIT", historyMoves.CodCardCredit);
            fbCommand.Parameters.AddWithValue("VALTOTAL", historyMoves.ValTotal);
            fbCommand.Parameters.AddWithValue("DATMOVE", historyMoves.DatMove);
            fbCommand.Parameters.AddWithValue("CODGAME", historyMoves.CodGame);
                        
            fbCommand.ExecuteNonQuery();
            fbCommand.Transaction.Commit();
            fbCommand.Dispose();
        }

        public void updateCardBalance(HistoryMove historyMove, string idtOperation)
        {
            BalanceContext balanceContext = new BalanceContext();


            int valOperation = 1;

            if (idtOperation == "Subtract")
            {
                valOperation = -1;
            }


            if (historyMove.CodCardCredit > 0)
            {
                Balance balance = balanceContext.Balances(historyMove.CodGame, historyMove.CodCardCredit)
                                                     .SingleOrDefault();

                balance.ValTotal = balance.ValTotal + (historyMove.ValTotal * valOperation);

                balanceContext.putBalances(balance);
            }


            if (historyMove.CodCardDebit > 0)
            {
                Balance balance = balanceContext.Balances(historyMove.CodGame, historyMove.CodCardDebit)
                                                     .SingleOrDefault();

                balance.ValTotal = balance.ValTotal + (historyMove.ValTotal * (valOperation * -1));

                balanceContext.putBalances(balance);
            }
        }
    }
}