﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirebirdSql.Data.FirebirdClient;
using System.Data;

namespace CalculatorMonopolyGame.Models
{
    public class BalanceContext
    {
        ConnectionDB connectionDB = new ConnectionDB();

        public IEnumerable<Balance> Balances (int codGame, int codCard)
        {
            List<Balance> balances = new List<Balance>();

            string commandText = @"SELECT * 
                                     FROM MON_BALANCE 
                                    WHERE CODGAME = @CODGAME
                                      AND (   @CODCARD = 0
                                           OR CODCARD = @CODCARD)
                                    ORDER BY CODBALANCE ";

            FbCommand fbCommand = connectionDB.prepareconnection(commandText);
            fbCommand.Parameters.AddWithValue("CODGAME", codGame);
            fbCommand.Parameters.AddWithValue("CODCARD", codCard);

            FbDataReader rdr = fbCommand.ExecuteReader();
            while (rdr.Read())
            {
                Balance balance = new Balance();
                balance.CodBalance = Convert.ToInt32(rdr["CodBalance"]);
                balance.CodGame = Convert.ToInt32(rdr["CodGame"]);
                balance.CodCard = Convert.ToInt32(rdr["CodCard"]);
                balance.ValTotal = float.Parse(rdr["ValTotal"].ToString());
                balance.ValStarted = float.Parse(rdr["ValStarted"].ToString());

                balances.Add(balance);
            }
                        
            fbCommand.Dispose();

            return balances;
        }

        public int postBalances(Balance balances)
        {

            var existBalance = Balances(balances.CodGame, balances.CodCard);

            if (existBalance.Count() > 0)
            {
                throw new Exception("The card exist to these balance.");
            }

            Balance balance = new Balance(balances.CodGame,
                                          balances.CodCard,
                                          balances.ValTotal,
                                          balances.ValStarted);

            

            string commandText = @"SELECT GEN_ID(GEN_MON_BALANCE_ID,1) AS CODBALANCE
                                     FROM DUAL";

            FbCommand fbSequence = connectionDB.prepareconnection(commandText);
            FbDataReader rdr = fbSequence.ExecuteReader();

            if (rdr.Read())
            {
                balance.CodBalance = Convert.ToInt32(rdr["CODBALANCE"]);
            }
                        
            fbSequence.Dispose();
            

            commandText = @"INSERT INTO MON_BALANCE (CODBALANCE, CODGAME, CODCARD, VALTOTAL, VALSTARTED) 
                                             VALUES (@CODBALANCE, @CODGAME, @CODCARD, @VALTOTAL, @VALSTARTED) ";

            FbCommand fbCommand = connectionDB.prepareconnection(commandText);
            fbCommand.Parameters.AddWithValue("CODBALANCE", balance.CodBalance);
            fbCommand.Parameters.AddWithValue("CODGAME", balance.CodGame);
            fbCommand.Parameters.AddWithValue("CODCARD", balance.CodCard);
            fbCommand.Parameters.AddWithValue("VALTOTAL", balance.ValTotal);
            fbCommand.Parameters.AddWithValue("VALSTARTED", balance.ValStarted);
                        
            fbCommand.ExecuteNonQuery();
            fbCommand.Transaction.Commit();            
            fbCommand.Dispose();

            return balance.CodBalance;
        }

        public void deleteBalances(int codBalances)
        {
            string commandText = @"DELETE FROM MON_BALANCE 
                                    WHERE CODBALANCE = @CODBALANCE ";

            FbCommand fbCommand = connectionDB.prepareconnection(commandText);
            fbCommand.Parameters.AddWithValue("CODBALANCE", codBalances);
                        
            fbCommand.ExecuteNonQuery();
            fbCommand.Transaction.Commit();            
            fbCommand.Dispose();
        }

        public void putBalances(Balance balances)
        {
            string commandText = @"UPDATE MON_BALANCE 
                                      SET CODGAME = @CODGAME, 
                                          CODCARD = @CODCARD, 
                                          VALTOTAL = @VALTOTAL, 
                                          VALSTARTED = @VALSTARTED 
                                    WHERE CODBALANCE = @CODBALANCE ";

            FbCommand fbCommand = connectionDB.prepareconnection(commandText);
            fbCommand.Parameters.AddWithValue("CODBALANCE", balances.CodBalance);
            fbCommand.Parameters.AddWithValue("CODGAME", balances.CodGame);
            fbCommand.Parameters.AddWithValue("CODCARD", balances.CodCard);
            fbCommand.Parameters.AddWithValue("VALTOTAL", balances.ValTotal);
            fbCommand.Parameters.AddWithValue("VALSTARTED", balances.ValStarted);
            
            fbCommand.ExecuteNonQuery();
            fbCommand.Transaction.Commit();            
            fbCommand.Dispose();
        }
    }
}