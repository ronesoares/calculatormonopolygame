﻿using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace CalculatorMonopolyGame.Models
{
    public class ConnectionDB
    {
        private String connectionDB
        {
            get
            {
                string connectionString = "User=SYSDBA;" +
                                          "Password=masterkey;" +
                                          "Database=C:\\Git\\CalculatorMonopolyGame\\CalculatorMonopolyGame\\MONOPOLY.FDB;" +
                                          "DataSource=RONE-PC;" +
                                          "Port=3050;" +
                                          "Dialect=3;" +
                                          "Charset=WIN1252;" +
                                          "Role=;" +
                                          "Connection lifetime=15;" +
                                          "Pooling=false;" +
                                          "MinPoolSize=0;" +
                                          "MaxPoolSize=50;" +
                                          "Packet Size=8192;" +
                                          "ServerType=0";

                return connectionString;
            }
        }

        public FbCommand prepareconnection(string commandText)
        {
            //FbConnection.ClearAllPools();
            FbConnection con = new FbConnection(connectionDB.ToString());
            con.Open();
            FbTransaction transaction = con.BeginTransaction();

            FbCommand fbCommand = new FbCommand();
            fbCommand.CommandText = commandText;
            fbCommand.CommandType = CommandType.Text;
            fbCommand.Connection = con;
            fbCommand.Transaction = transaction;

            return fbCommand;
        }
    }
}