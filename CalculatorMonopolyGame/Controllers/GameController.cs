﻿using CalculatorMonopolyGame.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CalculatorMonopolyGameAPI.Controllers
{
    [RoutePrefix("Game")]
    public class GameController : ApiController
    {

        private GameContext gameContext = new GameContext();

        [Route("List")]
        [HttpGet]
        public HttpResponseMessage GetGame()
        {
            return Request.CreateResponse(HttpStatusCode.OK, gameContext.Games);
        }


        [Route("List/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetGame(int id)
        {
            var game = gameContext.Games.Where(e => e.Id == id);

            if (game.Count() > 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, game);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Game with id = " + id + " not found");
            }
        }

        
        [Route("Insert")]
        [HttpPost]
        public HttpResponseMessage PostGame([FromBody] Game game)
        {
            try
            {
                game.Id = gameContext.postGames(game.Description);

                return Request.CreateResponse(HttpStatusCode.OK, game);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }


        [Route("Update")]
        [HttpPut]
        public HttpResponseMessage PutGame([FromBody] Game game)
        {
            try
            {
                gameContext.putGames(game);

                return Request.CreateResponse(HttpStatusCode.OK, game);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }


        [Route("Delete/{id:int}")]
        [HttpDelete]
        public HttpResponseMessage DeleteGame(int id)
        {
            try
            {
                gameContext.deleteGames(id);

                return Request.CreateResponse(HttpStatusCode.OK, id);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
