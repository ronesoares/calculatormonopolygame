﻿using CalculatorMonopolyGame.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CalculatorMonopolyGameAPI.Controllers
{
    [RoutePrefix("Card")]
    public class CardController : ApiController
    {

        private CardContext cardContext = new CardContext();

        [Route("List")]
        [HttpGet]
        public HttpResponseMessage GetCard()
        {
            return Request.CreateResponse(HttpStatusCode.OK, cardContext.Cards);
        }


        [Route("List/{codCard:int}")]
        [HttpGet]
        public HttpResponseMessage GetCard(int codCard)
        {
            var card = cardContext.Cards.Where(e => e.CodCard == codCard);

            if (card.Count() > 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, card);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Card with id = " + codCard + " not found");
            }
        }


        [Route("Insert")]
        [HttpPost]
        public HttpResponseMessage PostCard([FromBody] Card card)
        {
            try
            {
                card.CodCard = cardContext.postCards(card.DscCard);

                return Request.CreateResponse(HttpStatusCode.OK, card);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }


        [Route("Update")]
        [HttpPut]
        public HttpResponseMessage PutCard([FromBody] Card card)
        {
            try
            {
                cardContext.putCards(card);

                return Request.CreateResponse(HttpStatusCode.OK, card);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }


        [Route("Delete/{codCard:int}")]
        [HttpDelete]
        public HttpResponseMessage DeleteCard(int codCard)
        {
            try
            {
                cardContext.deleteCards(codCard);

                return Request.CreateResponse(HttpStatusCode.OK, codCard);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
