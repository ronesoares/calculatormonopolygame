﻿using CalculatorMonopolyGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CalculatorMonopolyGame.Controllers
{
    [RoutePrefix("Balance")]
    public class BalanceController : ApiController
    {
        private BalanceContext balanceContext = new BalanceContext();


        [Route("List/{codGame:int}")]
        [HttpGet]
        public HttpResponseMessage GetBalance(int codGame)
        {
            var balance = balanceContext.Balances(codGame, 0);

            return Request.CreateResponse(HttpStatusCode.OK, balance);
        }


        [Route("Insert")]
        [HttpPost]
        public HttpResponseMessage PostBalance([FromBody] Balance balance)
        {
            try
            {
                balanceContext.postBalances(balance);

                return Request.CreateResponse(HttpStatusCode.OK, balance);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }


        [Route("Update")]
        [HttpPut]
        public HttpResponseMessage PutBalance([FromBody] Balance balance)
        {
            try
            {
                balanceContext.putBalances(balance);

                return Request.CreateResponse(HttpStatusCode.OK, balance);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }


        [Route("Delete/{codBalance:int}")]
        [HttpDelete]
        public HttpResponseMessage DeleteBalance(int codBalance)
        {
            try
            {
                balanceContext.deleteBalances(codBalance);

                return Request.CreateResponse(HttpStatusCode.OK, codBalance);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
