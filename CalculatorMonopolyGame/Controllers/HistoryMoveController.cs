﻿using CalculatorMonopolyGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CalculatorMonopolyGame.Controllers;

namespace CalculatorMonopolyGame.Controllers
{
    [RoutePrefix("HistoryMove")]
    public class HistoryMoveController : ApiController
    {
        private HistoryMoveContext historyMoveContext = new HistoryMoveContext();


        [Route("List/{codGame:int}")]
        [HttpGet]
        public HttpResponseMessage GetHistoryMove(int codGame)
        {
            var historyMove = historyMoveContext.HistoryMoves(codGame, 0);

            return Request.CreateResponse(HttpStatusCode.OK, historyMove);            
        }


        [Route("Insert")]
        [HttpPost]
        public HttpResponseMessage PostHistoryMove([FromBody] HistoryMove historyMove)
        {
            try
            {
                historyMoveContext.postHistoryMoves(historyMove);

                historyMoveContext.updateCardBalance(historyMove, "Sum");

                return Request.CreateResponse(HttpStatusCode.OK, historyMove);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }


        [Route("Update")]
        [HttpPut]
        public HttpResponseMessage PutHistoryMove([FromBody] HistoryMove historyMove)
        {
            try
            {
                HistoryMove updatehistoryMove = historyMoveContext.
                                                 HistoryMoves(0, historyMove.CodHistoryMove)
                                                              .SingleOrDefault();

                historyMoveContext.updateCardBalance(updatehistoryMove, "Subtract");

                historyMoveContext.putHistoryMoves(historyMove);

                historyMoveContext.updateCardBalance(historyMove, "Sum");

                return Request.CreateResponse(HttpStatusCode.OK, historyMove);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }


        [Route("Delete/{codHistoryMove:int}")]
        [HttpDelete]
        public HttpResponseMessage DeleteHistoryMove(int codHistoryMove)
        {
            try
            {
                HistoryMove historyMove = historyMoveContext.HistoryMoves(0, codHistoryMove).SingleOrDefault();

                historyMoveContext.deleteHistoryMoves(codHistoryMove);                

                historyMoveContext.updateCardBalance(historyMove, "Subtract");

                return Request.CreateResponse(HttpStatusCode.OK, codHistoryMove);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
